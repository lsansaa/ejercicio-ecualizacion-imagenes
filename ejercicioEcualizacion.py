import argparse
import matplotlib.pyplot as plt
import numpy as np
import cv2

from numpy import *
from time import time
from PIL import Image


def histeq_bw(im,nbr_bins=256):
    """ Ecualizacion por histograma de una imagen. """
    # Calcular el histograma de la imagen
    imhist,bins = np.histogram(im.flatten(),nbr_bins,normed=True)
    cdf = imhist.cumsum() # Funcion de distribucion acumulativa
    cdf_norm = 255*cdf / cdf[-1] # Normalizar
    # Usar interpolacion lineal para encontrar los nuevos puntos
    im2 = np.interp(im.flatten(),bins[:-1],cdf_norm)
    im2hist,bins2 = np.histogram(im2.reshape(im.shape).flatten(),nbr_bins,normed=True)
    return im2.reshape(im.shape),im2hist, imhist, cdf, bins[:-1]

def histogram_bw(image_dir):
    #Imagen como vector numpy
    # image = cv2.imread(image_dir)
    image = np.array(Image.open(image_dir).convert('L'))
    image_color = np.array(Image.open(image_dir).convert('L'))
    #Ecualizacion de imagen
    im_bw,imhisteq_bw, imhisteq, cdf, bins = histeq_bw(image, 256)

    # Creacion de graficos
    fig, axes = plt.subplots(nrows=1, ncols=3)
    ax0, ax1, ax2= axes.flatten()

    # print time()
    ax0.hist(imhisteq, bins='auto')
    ax0.set_title("Histograma imagen")
    
    ax1.plot(bins, cdf)
    ax1.set_title("Funcion acumulativa")
    
    ax2.hist(imhisteq_bw, bins='auto')
    ax2.set_title("Histograma imagen Eq.")

    fig.tight_layout()
    plt.show()

    # concatenacion de imagenes
    # numpy_horizontal = np.hstack((image, im_bw))
    # mostrar imagenes
    img = Image.fromarray(im_bw).convert("L")
    img.convert('RGB')
    img.save('resultBW1.jpg')
    img.show()


def histogram_color(image_dir):
    # Imagen como vector numpy
    image = Image.open(image_dir)
    # ancho y largo de la imagen
    width, height = image.size
    #separar canales rgb
    r, g, b = image.split()
    #canales rgb como arrays
    li_r=list(image.getdata(band=0))
    r_image = np.array(li_r,dtype="uint8")
    li_g=list(image.getdata(band=1))
    g_image = np.array(li_g,dtype="uint8")
    li_b=list(image.getdata(band=2))
    b_image = np.array(li_b,dtype="uint8")
    #Ecualizacion de canal rojo
    im_red,imhisteq_red, imhist_red, cdf_red, bins_red = histeq_bw(r_image, 256)
    #Ecualizacion de canal verde
    im_green,imhisteq_green, imhist_green, cdf_green, bins_green = histeq_bw(g_image, 256)
    #Ecualizacion de canal azul
    im_blue,imhisteq_blue, imhist_blue, cdf_blue, bins_blue = histeq_bw(b_image, 256)
    # Creacion de graficos
    fig, axes = plt.subplots(nrows=3, ncols=3)
    ax0, ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8= axes.flatten()

    ax0.hist(imhist_red, bins='auto', color="red")
    ax0.set_title("Histograma imagen rojo")
    
    ax1.plot(bins_red, cdf_red, color="red")
    ax1.set_title("Funcion acumulativa rojo")
    
    ax2.hist(imhisteq_red, bins='auto', color="red")
    ax2.set_title("Histograma imagen Eq. rojo")

    ax3.hist(imhist_green, bins='auto', color="green")
    ax3.set_title("Histograma imagen verde")
    
    ax4.plot(bins_green, cdf_green, color="green")
    ax4.set_title("Funcion acumulativa verde")
    
    ax5.hist(imhisteq_green, bins='auto', color="green")
    ax5.set_title("Histograma imagen Eq. verde")

    ax6.hist(imhist_blue, bins='auto', color="blue")
    ax6.set_title("Histograma imagen azul")
    
    ax7.plot(bins_blue, cdf_blue, color="blue")
    ax7.set_title("Funcion acumulativa azul")
    
    ax8.hist(imhisteq_blue, bins='auto', color="blue")
    ax8.set_title("Histograma imagen Eq. azul")

    fig.tight_layout()
    plt.show()

    # reformar canales y cambiar dtype
    im_red = np.array(im_red,dtype="uint8")
    im_green = np.array(im_green,dtype="uint8")
    im_blue = np.array(im_blue,dtype="uint8")
    reshaper = im_red.reshape(height,width)
    reshapeg = im_green.reshape(height,width)
    reshapeb = im_blue.reshape(height,width)
    # cargar canales en distintas imagenes
    imr=Image.fromarray(reshaper,mode=None)
    imb=Image.fromarray(reshapeb,mode=None)
    img=Image.fromarray(reshapeg,mode=None)
    # combinar imagenes de canales y mostrar resultado
    merged=Image.merge("RGB",(imr,img,imb))
    merged.save('resultRGB1.png',"PNG")
    merged.show()


def main(image_dir):
    histogram_bw(image_dir)
    histogram_color(image_dir)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Ejercicio de ecualizacion de imagenes, a color y en blanco y negro. Indique la direccion de una imagen.')
    parser.add_argument(
        'image_directory', help='Directorio de imagen a ecualizar')
    args = parser.parse_args()
    t0 = time()
    print 'Program started'
    main(args.image_directory)
    print("Program done in %0.3fs" % (time() - t0))